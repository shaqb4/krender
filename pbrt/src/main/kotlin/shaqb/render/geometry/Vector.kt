package shaqb.render.geometry


data class Vector2<T : Number>(var x: T, var y: T) {
    
    init {
        if (hasNaN())
            throw Error("Vector2 has NaN")
    }
    
    operator fun get(i: Int) = when(i) {
        0 -> this.x
        1 -> this.y
        else -> throw Error("$i is not a valid Vector2 index")
    }
    
    operator fun set(i: Int, el: T) = when(i) {
        0 -> this.x = el
        1 -> this.y = el
        else -> throw Error("$i is not a valid Vector2 index")
    }
    
    private fun hasNaN() = when (x) {
        is Float -> x.equals(Float.NaN) || y.equals(Float.NaN)
        is Double -> x.equals(Double.NaN) || y.equals(Double.NaN)
        else -> false
    }
    
    fun toDoubleVector() = map { it.toDouble() }
    fun toFloatVector() = map { it.toFloat() }
    fun toIntVector() = map { it.toInt() }
    fun toLongVector() = map { it.toLong() }
    
    companion object {
        val Zerof = Vector2<Float>(0f, 0f)
        val Zerod = Vector2<Double>(0.0, 0.0)
        val Zeroi = Vector2<Int>(0, 0)
        val Zerol = Vector2<Long>(0, 0)         
    }
}

inline public fun <T : Number, R : Number> Vector2<T>.map(transform: (T) -> R)= Vector2<R>(transform(x), transform(y))

inline fun <T : Number> Vector2<out T>.forEach(action: (T) -> Unit) {
    action(x)
    action(y)
}

inline fun <T : Number> Vector2<out T>.forEachIndexed(action: (index: Int, T) -> Unit) {
    action(0, x)
    action(1, y)
}

inline fun <T : Number, V : Number> Vector2<out T>.zip(
    other: Vector2<out T>, 
    transform: (a: T, b: T) -> V
) = Vector2<V>(transform(x, other.x), transform(y, other.y))


typealias Vec2f = Vector2<Float>
operator fun Vec2f.plus(other: Vec2f) = this.zip(other) { a, b -> a + b }
operator fun Vec2f.plusAssign(other: Vec2f) = this.forEachIndexed { index, it -> this[index] = it + other[index] }

operator fun Vec2f.times(s: Float) = map { it * s }//Vec2f(x * s, y * s)
operator fun Vec2f.timesAssign(s: Float) {
    this.x *= s
    this.y *= s
}

typealias Vec2d = Vector2<Double>
operator fun Vec2d.plus(other: Vec2d) = Vec2d(x+other.x, y + other.y)
operator fun Vec2d.plusAssign(other: Vec2d) {
    this.x += other.x
    this.y += other.y
}
operator fun Vec2d.times(s: Double) = Vec2d(x * s, y * s)
operator fun Vec2d.timesAssign(s: Double) {
    this.x *= s
    this.y *= s
}

typealias Vec2i = Vector2<Int>
operator fun Vec2i.plus(other: Vec2i) = Vec2i(x+other.x, y + other.y)
operator fun Vec2i.plusAssign(other: Vec2i) {
    this.x += other.x
    this.y += other.y
}
operator fun Vec2i.times(s: Int) = Vec2i(x * s, y * s)
operator fun Vec2i.timesAssign(s: Int) {
    this.x *= s
    this.y *= s
}

typealias Vec2l = Vector2<Long>
operator fun Vec2l.plus(other: Vec2l) = Vec2l(x+other.x, y + other.y)
operator fun Vec2l.plusAssign(other: Vec2l) {
    this.x += other.x
    this.y += other.y
}
operator fun Vec2l.times(s: Long) = Vec2l(x * s, y * s)
operator fun Vec2l.timesAssign(s: Long) {
    this.x *= s
    this.y *= s
}



data class Vector3<T : Number>(var x: T, var y: T, var z: T) {
    
    init {
        if (hasNaN())
            throw Error("Vector3 has NaN")
    }
    
    operator fun get(i: Int) = when(i) {
        0 -> this.x
        1 -> this.y
        2 -> this.z
        else -> throw Error("$i is not a valid Vector3 index")
    }
    
    operator fun set(i: Int, el: T) = when(i) {
        0 -> this.x = el
        1 -> this.y = el
        2 -> this.z = el
        else -> throw Error("$i is not a valid Vector3 index")
    }
    
    private fun hasNaN() = when (x) {
        is Float -> x.equals(Float.NaN) || y.equals(Float.NaN) || z.equals(Float.NaN)
        is Double -> x.equals(Double.NaN) || y.equals(Double.NaN) || z.equals(Double.NaN)
        else -> false
    }
    
    fun toDoubleVector() = map { it.toDouble() }
    fun toFloatVector() = map { it.toFloat() }
    fun toIntVector() = map { it.toInt() }
    fun toLongVector() = map { it.toLong() }
    
         
    
    companion object {
        val Zerof = Vector3<Float>(0f, 0f, 0f)
        val Zerod = Vector3<Double>(0.0, 0.0, 0.0)
        val Zeroi = Vector3<Int>(0, 0, 0)
        val Zerol = Vector3<Long>(0, 0, 0)         
    }
}

inline public fun <T : Number, R : Number> Vector3<T>.map(transform: (T) -> R)= Vector3<R>(transform(x), transform(y), transform(z))

inline fun <T : Number> Vector3<out T>.forEach(action: (T) -> Unit) {
    action(x)
    action(y)
    action(z)
}


typealias Vec3f = Vector3<Float>
operator fun Vec3f.plus(other: Vec3f) = Vec3f(x + other.x, y + other.y, z + other.z)
operator fun Vec3f.plusAssign(other: Vec3f) {
    this.x += other.x
    this.y += other.y
    this.z += other.z
}
operator fun Vec3f.times(s: Float) = Vec3f(x * s, y * s, z * s)
operator fun Vec3f.timesAssign(s: Float) {
    this.x *= s
    this.y *= s
    this.z *= s
}

typealias Vec3d = Vector3<Double>
operator fun Vec3d.plus(other: Vec3d) = Vec3d(x + other.x, y + other.y, z + other.z)
operator fun Vec3d.plusAssign(other: Vec3d) {
    this.x += other.x
    this.y += other.y
    this.z += other.z
}
operator fun Vec3d.times(s: Double) = Vec3d(x * s, y * s, z * s)
operator fun Vec3d.timesAssign(s: Double) {
    this.x *= s
    this.y *= s
    this.z *= s
}

typealias Vec3i = Vector3<Int>
operator fun Vec3i.plus(other: Vec3i) = Vec3i(x + other.x, y + other.y, z + other.z)
operator fun Vec3i.plusAssign(other: Vec3i) {
    this.x += other.x
    this.y += other.y
    this.z += other.z
}
operator fun Vec3i.times(s: Int) = Vec3i(x * s, y * s, z * s)
operator fun Vec3i.timesAssign(s: Int) {
    this.x *= s
    this.y *= s
    this.z *= s
}

typealias Vec3l = Vector3<Long>
operator fun Vec3l.plus(other: Vec3l) = Vec3l(x + other.x, y + other.y, z + other.z)
operator fun Vec3l.plusAssign(other: Vec3l) {
    this.x += other.x
    this.y += other.y
    this.z += other.z
}
operator fun Vec3l.times(s: Long) = Vec3l(x * s, y * s, z * s)
operator fun Vec3l.timesAssign(s: Long) {
    this.x *= s
    this.y *= s
    this.z *= s
}
